# Core
SkyWire Core contains a collection of integration and helper libraries to utilize a SkyWire Module.

## Purpose
SkyWire Core allows for third parties to integrate with a SkyWire Module.

## Requirements
* Dotnet Standard 2.0 or higher

## Installation
`SkyWire.Core` is available as a nuget package from the package manager console:
```
#!ps
Install-Package SkyWire.Core
```

## Usage
For an interface to integrate with a SkyWire Module, `IModuleConfiguration` must be implemented inside the package.
```
#!c#
using SkyWire.Core;

[Export(typeof(IConfigurationModule))]
[Shared]
[ExportMetadata("Company", "SkyWire")]
[ExportMetadata("Name", "sw_creditcard")]
public class SkyWireModuleConfiguration : IConfigurationModule
{
    [DisplayName("Server")]
    [Description("IP of credit card server")]
    public string Server { get; set; }
}
```

### Module Configuration Options
We support attributes for validation on properties during configuration. List of supported attributes are:

* [DisplayNameAttribute](https://msdn.microsoft.com/en-us/library/system.componentmodel.displaynameattribute(v=vs.110).aspx)
* [Description](https://msdn.microsoft.com/en-us/library/system.componentmodel.descriptionattribute(v=vs.110).aspx)
* [EnumDataType](https://msdn.microsoft.com/en-us/library/system.componentmodel.dataannotations.enumdatatypeattribute(v=vs.110).aspx)
* [RegularExpression](https://msdn.microsoft.com/en-us/library/system.componentmodel.dataannotations.regularexpressionattribute(v=vs.110).aspx)
* [Required](https://msdn.microsoft.com/en-us/library/system.componentmodel.dataannotations.requiredattribute(v=vs.110).aspx)
* [Url](https://msdn.microsoft.com/en-us/library/system.componentmodel.dataannotations.urlattribute(v=vs.110).aspx)

Note: If you need a custom validator `IValidateObject` can be implemented where `Validate` would be called when attempting to save the configuration.
